import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "src/types/Products";

export const useProductStore = defineStore("Product", () => {
  const total = ref(0);
  const Selected = ref(<Product[]>[]);
  const add = (id: number, name: string, price: number) => {
    total.value += price;
    Selected.value.push({ id, name, price });
  };
  return { total, add, Selected };
});
